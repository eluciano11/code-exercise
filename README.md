###Database
+ Create a local Postgres Database with the name `exercise`
+ The database should run on `127.0.0.1:5432`

##Project Setup
+ Install [virtualenvwrapper](http://virtualenvwrapper.readthedocs.io/en/latest/) on your machine
+ Make your virtualenv `mkvirtualenv <env-name>`
+ Start virtualenv `workon <env-name>`
+ Run `pip install -r requirements.txt`
+ Run server `./manage.py runserver_plus`
