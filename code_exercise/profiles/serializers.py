from rest_framework import serializers
from rest_framework.authtoken.models import Token

from .models import User


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        base_key = 'user'
        fields = ('id', 'username',)


class SignupSerializer(serializers.ModelSerializer):
    class Meta:
        model = User

    def create_user(self, data):
        username = data['username']
        password = data['password']

        return User.objects.create_user(username=username, password=password)

    def validate(self, validated_data):
        user = self.create_user(validated_data)
        Token.objects.create(user=user)

        return UserSerializer(user).data
