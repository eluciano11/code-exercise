from django.conf.urls import patterns, url

from rest_framework.authtoken import views

from .views import SignupView

urlpatterns = patterns(
    '',
    url(
        r'^signup/$',
        SignupView.as_view()
    ),
    url(r'^login/$', views.obtain_auth_token)
)
