from django.db import models

from ..profiles.models import User


class Picture(models.Model):
    title = models.CharField(max_length=30)
    url = models.CharField(max_length=200)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
