from django.conf.urls import patterns, url

from .views import AddPictureView, MyPicturesView, AllPicturesView

urlpatterns = patterns(
    '',
    url(
        r'^add/$',
        AddPictureView.as_view()
    ),
    url(
        r'^my-pictures/$',
        MyPicturesView.as_view()
    ),
    url(
        r'^all/$',
        AllPicturesView.as_view()
    )
)
