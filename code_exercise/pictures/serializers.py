from rest_framework import serializers

from .models import Picture


class AddPictureSerializer(serializers.ModelSerializer):
    class Meta:
        model = Picture


class MyPicturesSerializers(serializers.ModelSerializer):
    class Meta:
        model = Picture


class AllPicturesSerializers(serializers.ModelSerializer):
    class Meta:
        model = Picture
