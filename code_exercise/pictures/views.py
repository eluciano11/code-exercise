from rest_framework import generics

from .serializers import (AddPictureSerializer, MyPicturesSerializers,
                          AllPicturesSerializers)


class AddPictureView(generics.CreateAPIView):
    serializer_class = AddPictureSerializer


class MyPicturesView(generics.ListAPIView):
    serializer_class = MyPicturesSerializers


class AllPicturesView(generics.ListAPIView):
    serializer_class = AllPicturesSerializers
