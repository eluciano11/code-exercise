from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns(
    '',
    url(r'^admin/', include(admin.site.urls)),
    url(
        r'^api-auth/',
        include('rest_framework.urls', namespace='rest_framework')
    ),
    url(
        r'^api/users/',
        include('code_exercise.profiles.urls', namespace='profiles')
    ),
    url(
        r'^api/pictures/',
        include('code_exercise.pictures.urls', namespace='pictures')
    )
)
